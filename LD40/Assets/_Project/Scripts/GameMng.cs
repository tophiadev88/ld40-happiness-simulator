﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public enum GameStates {
	Null,
	Playing,
	GameEnd,
	GameEndedWithZeroHappiness,
	GameEndedWithZeroGold
}

public enum PhaseStates {
	Null,
	TurnStart,
	TurnEnd
}

public class GameMng : MonoBehaviour {
	public static bool isInit = false;
	public const string LogPrefix = "<b>[GameMng]:</b> ";
	private int _turn;
	private bool _isFreeze;
	private float _lastMoneyExp;
	private float _lastDreamExp;
	private int _lastGold;
	private int _lastHappiness;
	private int _turnRemaining;
	private bool _isDanger;
	private int _workForMoneyInRow;
	private AudioSource _audioSource;
	public int turn {
		get { return _turn; }
		set { _turn = value; OnTurnChanged (_turn); }
	}

	public int testAmount = 10000;
	public int turnRemaining {
		get { return _turnRemaining; }
		set {
			_turnRemaining = value;

			Debug.Log (LogPrefix + "Turn Remaining: " + _turnRemaining);

			if (_turnRemaining == 0) {
				CheckGameOver ();
			}
		}
	}
	public string testLabel;

	public GameStates gameState;
	public PhaseStates phaseState;
	public int turnToEndGame = 100;
	public PlayerData player;
	public WorkData moneyWork;
	public WorkData dreamWork;
	public UIMng uIMng;
	public UIGameOverMng uIGameOverMng;
	public CameraFollow cameraFollow;
	public Transform previewGroup;
	public Transform roomGroup;
	public bool isFreeze {
		get { return _isFreeze; }
		set {
			_isFreeze = value;
			uIMng.SetInteractable (!_isFreeze);
		}
	}

	public bool isDanger {
		get { return _isDanger; }
		set {
			if (_isDanger != value) {
				_isDanger = value;

				if (player.currentGold <= 0) {
					uIMng.popupGroup.ShowText ("LAST MOVE!! You're run out of gold!");
					uIMng.GAlert (true);
				} else if (player.happiness.current <= 0) {
					uIMng.popupGroup.ShowText ("LAST MOVE!! You're feeling depression!");
					uIMng.HAlert (true);
				} else {
					uIMng.GAlert (false);
					uIMng.HAlert (false);
				}

				if (_isDanger) {
					_turnRemaining = 2;
					_audioSource.PlayOneShot (Resources.Load<AudioClip> ("ambient-alert"));
				} else {
					_turnRemaining = -1;
				}

				Debug.Log (LogPrefix + "Danger: " + _isDanger);

				cameraFollow.SetDanger (_isDanger);
			}
		}
	}

	private void Awake () {
		if (!isInit) {
			isInit = true;
			StartCoroutine (PlayAudioDelay ());
		} else {
			GameObject.Find ("Theme").SetActive (false);
			GetComponent<AudioSource> ().Play ();
		}
		uIMng = FindObjectOfType<UIMng> ();
		uIGameOverMng = FindObjectOfType<UIGameOverMng> ();
		cameraFollow = FindObjectOfType<CameraFollow> ();
		_audioSource = GetComponent<AudioSource> ();

		GameData.Init ();

		moneyWork = GameData.GetMoneyWork ();
		dreamWork = GameData.GetDreamWork ();

		previewGroup = transform.Find ("PreviewGroup");
		roomGroup = transform.Find ("RoomGroup");

		_turnRemaining = -1;
	}

	private void Start () {
		uIMng.mBtn.onClick += WorForMoney;
		uIMng.dBtn.onClick += WorkForDream;
		uIMng.onBuyItem += OnBuyItem;

		uIGameOverMng.restartBtn.onClick.AddListener (Restart);

		CreatePlayer ();
		SetupStore ();
		SetTurn (1);
		SetDreamLv (1);
		SetMoneyLv (1);
		UpdateUI ();
		SetGameState (GameStates.Playing);
		phaseState = PhaseStates.TurnStart;
	}

	IEnumerator PlayAudioDelay () {
		yield return new WaitForSeconds (6f);
		var src = GetComponent<AudioSource> ();
		src.Play ();
		src.DOFade (1f, 1f);
	}

	private void MoneyLevelUp () {
		SetMoneyLv (player.levelForMoney + 1);
		uIMng.popupGroup.ShowText ("Working for money level UP!");
		_audioSource.PlayOneShot (Resources.Load<AudioClip> ("snd-lvl-up"));
	}

	private void DreamLevelUp () {
		SetDreamLv (player.levelForDream + 1);

		if (gameState == GameStates.Playing)
			uIMng.popupGroup.ShowText ("Working for Dream level UP!");

		_audioSource.PlayOneShot (Resources.Load<AudioClip> ("snd-lvl-up"));
	}

	public void Restart () {
		UnityEngine.SceneManagement.SceneManager.LoadScene (0);
	}

	public void SetDreamLv (int lv) {
		player.levelForDream = lv;

		var d = dreamWork.GetDataFromLevel (player.levelForDream + 1);
		player.livingCost.gold -= d.livingCost;

		if (d != null) {
			uIMng.dBtn.expBar.SetRange (_lastDreamExp, d.amountToLvUp);
			_lastDreamExp = d.amountToLvUp;
		}

		player.livingCost.gold -= d.livingCost;

		if (player.levelForDream == 3)
			uIMng.questionImg.DOFade (1f, 1f);

		else if (player.levelForDream == 5) {
			uIMng.DropFaskToDoList ();
			SetGameState (GameStates.GameEnd);
		}

		UpdateUI ();
	}

	public void SetMoneyLv (int lv) {
		player.levelForMoney = lv;

		var d = moneyWork.GetDataFromLevel (player.levelForMoney);
		player.livingCost.gold -= d.livingCost;

		d = moneyWork.GetDataFromLevel (player.levelForMoney + 1);
		if (d != null) {
			uIMng.mBtn.expBar.SetRange (_lastMoneyExp, d.amountToLvUp);
			_lastMoneyExp = d.amountToLvUp;
		}

		UpdateUI ();
	}

	private void UpdateUI () {
		uIMng.dBtn.UpdateData (dreamWork.GetDataFromLevel (player.levelForDream));
		uIMng.dBtn.SetLv (player.levelForDream);

		uIMng.mBtn.UpdateData (moneyWork.GetDataFromLevel (player.levelForMoney));
		uIMng.mBtn.SetLv (player.levelForMoney);
	}

	private void SetupStore () {
		uIMng.AddItems (ItemTypes.Girl, GameData.GetGirlDatas ());
		uIMng.AddItems (ItemTypes.Car, GameData.GetCarDatas ());
	}

	private void Update () {
		// Time.timeScale = (Input.GetKey (KeyCode.LeftControl)) ? 5 : 1;

		testLabel = testAmount.KiloFormat ();
		switch (gameState) {
			case GameStates.Playing:
				if (_lastGold != player.currentGold) {
					int cost = player.currentGold - _lastGold;
					uIMng.SetCurrentGold (player.currentGold);
					uIMng.goldCostDisplay.numberToDisplay = cost;
					uIMng.goldCostDisplay.Show ();
					_lastGold = player.currentGold;
				}

				if (_lastHappiness != player.happiness.current) {
					int cost = player.happiness.current - _lastHappiness;
					uIMng.happinessBar.Set (player.happiness);
					uIMng.happinessCostDisplay.numberToDisplay = cost;
					uIMng.happinessCostDisplay.Show ();
					_lastHappiness = player.happiness.current;
				}

				// Update exp bar
				uIMng.mBtn.expBar.SetValue (player.workForMoneyAmount);
				uIMng.dBtn.expBar.SetValue (player.workForDreamAmount);

				uIMng.SetLivingCost (player.livingCost);

				if (_turn >= turnToEndGame) {
					SetGameState (GameStates.GameEnd);
				}

				break;
		}

	}

	public void SetGameState (GameStates gameState) {
		this.gameState = gameState;

		isFreeze = gameState != GameStates.Playing;

		if (gameState == GameStates.GameEndedWithZeroGold ||
			gameState == GameStates.GameEndedWithZeroHappiness) {

			uIGameOverMng.SetGameOver (gameState);
		}

		if (gameState == GameStates.GameEnd) {
			StartCoroutine (GameEndDelay ());
		}

		Debug.Log (LogPrefix + " GameStates: " + this.gameState);
	}

	IEnumerator GameEndDelay () {
		var src = GetComponent<AudioSource> ();
		src.DOFade (0f, 1f);
		yield return new WaitForSeconds (2.0f);
		uIMng.GameFinish ();
	}

	public void SetPhaseState (PhaseStates phaseState) {
		this.phaseState = phaseState;

		switch (phaseState) {
			case PhaseStates.TurnStart:
				isFreeze = false;
				turn++;
				SetGameState (GameStates.Playing);
				break;

			case PhaseStates.TurnEnd:
				isFreeze = true;
				StartCoroutine (TurnEndCoroutine ());
				break;
		}

		Debug.Log (LogPrefix + " PhaseStates: " + this.phaseState);
	}

	IEnumerator TurnEndCoroutine () {
		if (player.ownedItems.Count > 0) {
			foreach (var i in player.ownedItems) {
				var d = GameData.GetItemData (i.itemTypes, i.id);
				i.Triggered (d.effectPerTurn);
			}
		}

		yield return new WaitForSeconds (0.5f);

		player.UpdatePerTurn (this);

		isDanger = player.currentGold <= 0 || player.happiness.current <= 0;

		SetPhaseState (PhaseStates.TurnStart);
	}

	private void CreatePlayer () {
		player = new PlayerData ();
	}

	private void CheckGameOver () {
		if (player.currentGold <= 0) SetGameState (GameStates.GameEndedWithZeroGold);
		else if (player.happiness.current <= 0) SetGameState (GameStates.GameEndedWithZeroHappiness);
	}

	void SetTurn (int turn) {
		_turn = turn;
		uIMng.SetTurnText (turn);
	}

	void OnTurnChanged (int turn) {
		Debug.Log (LogPrefix + "On Turn Changed: " + turn);
		uIMng.SetTurnText (turn);

		if (turnRemaining > 0) turnRemaining--;
	}

	// ===============================================================
	// BUTTON EVENTS
	// ===============================================================
	private void OnBuyItem (ItemTypes itemTypes, int id) {
		if (phaseState != PhaseStates.TurnStart || isFreeze) return;

		var d = GameData.GetItemData (itemTypes, id);
		var errors = d.CheckCondition (player);

		if (errors.Count > 0) {
			Debug.Log (errors.ToListString ());
		} else {
			isFreeze = true;
			var item = GameData.CreateItem (itemTypes, id, roomGroup);
			player.AddItems (item);
			StartCoroutine (FocusOnItemBought (item));
		}
	}
	void WorForMoney () {
		if (phaseState != PhaseStates.TurnStart || isFreeze) return;

		Debug.Log (LogPrefix + "On Click Money Btn");
		OnUpdateValue (moneyWork, player.levelForMoney);
		player.workForMoneyAmount++;
		_workForMoneyInRow++;
		// Check if fans leaving
		if (player.ownedFans.Count > 0 && _workForMoneyInRow >= 2) {
			var leaving = new List<Item> ();
			foreach (var f in player.ownedFans) {
				if (UnityEngine.Random.value <= 0.2f) {
					leaving.Add (f);
				}
			}

			if (leaving.Count != 0) {
				isFreeze = true;
				StartCoroutine (FansLeavingUpdate (leaving));
				return;
			}
		}
		var d = moneyWork.GetDataFromLevel (player.levelForMoney + 1);
		if (d != null && d.CheckCondition (player.workForMoneyAmount)) {
			MoneyLevelUp ();
		}
		SetPhaseState (PhaseStates.TurnEnd);
	}

	IEnumerator FansLeavingUpdate (List<Item> leavingFans) {
		foreach (var f in leavingFans) {
			player.RemoveItems (f);
			f.Leave ();
		}

		var str = string.Format ("There are {0} fans left because you rest for you dream!", leavingFans.Count);
		uIMng.popupGroup.ShowText (str);

		yield return new WaitForSeconds (1.0f);

		var d = moneyWork.GetDataFromLevel (player.levelForMoney + 1);
		if (d.CheckCondition (player.workForMoneyAmount)) {
			MoneyLevelUp ();
		}
		SetPhaseState (PhaseStates.TurnEnd);
	}

	void WorkForDream () {
		if (phaseState != PhaseStates.TurnStart || isFreeze) return;

		_workForMoneyInRow = 0;

		Debug.Log (LogPrefix + "On Click Dream Btn");
		OnUpdateValue (dreamWork, player.levelForDream);
		player.workForDreamAmount++;

		// Check if get fans
		var d = dreamWork.GetDataFromLevel (player.levelForDream);
		if (d.GetFans ()) {
			isFreeze = true;
			// Create fans
			var item = GameData.CreateItem (ItemTypes.Fan, 0, roomGroup);
			player.AddItems (item);
			StartCoroutine (GetFansDelay (item));
		} else {
			d = dreamWork.GetDataFromLevel (player.levelForDream + 1);
			if (d != null && d.CheckCondition (player.workForDreamAmount)) {
				DreamLevelUp ();
			}
			// Check if lvl up
			SetPhaseState (PhaseStates.TurnEnd);
		}
	}

	IEnumerator GetFansDelay (Item item) {
		var d = GameData.GetItemData (item.itemTypes, item.id);
		uIMng.popupGroup.ShowText ("You get more fans because of your effort to dream!");
		cameraFollow.SetTarget (item.transform);
		_audioSource.PlayOneShot (Resources.Load<AudioClip> ("snd-show"));
		yield return new WaitForSeconds (2f);
		player.ApplyInstantEffect (d.instantEffect);
		player.ApplyEffectPerTurn (d.effectPerTurn);
		yield return new WaitForSeconds (1f);
		cameraFollow.SetTarget (null);
		yield return new WaitForSeconds (0.5f);

		// Continue level up stuff
		var w = dreamWork.GetDataFromLevel (player.levelForDream + 1);
		if (w.CheckCondition (player.workForDreamAmount)) {
			DreamLevelUp ();
		}
		// Check if lvl up
		SetPhaseState (PhaseStates.TurnEnd);
	}

	// ===============================================================
	// PRIVATE METHODS
	// ===============================================================
	void OnUpdateValue (WorkData workData, int level) {
		workData.UpdatePlayerValue (player, level);
	}

	IEnumerator FocusOnItemBought (Item item) {
		var d = GameData.GetItemData (item.itemTypes, item.id);
		var format = string.Empty;
		if (item.itemTypes == ItemTypes.Girl) {
			format = "{0} has became your girlfriend!";
		} else {
			format = "You bought {0}, you felt happiness for the moment of Purchased!";
		}

		uIMng.popupGroup.ShowText (string.Format (format, d.itemName));
		cameraFollow.SetTarget (item.transform);
		_audioSource.PlayOneShot (Resources.Load<AudioClip> ("snd-show"));
		yield return new WaitForSeconds (2f);
		player.ApplyInstantEffect (d.instantEffect);
		player.ApplyEffectPerTurn (d.effectPerTurn);
		yield return new WaitForSeconds (1f);
		cameraFollow.SetTarget (null);
		yield return new WaitForSeconds (0.5f);

		if (!player.girlfriend && item.itemTypes == ItemTypes.Girl) {
			uIMng.slashA.DOFade (1f, 1f);
		}

		if (!player.carBought && item.itemTypes == ItemTypes.Car) {
			uIMng.slashB.DOFade (1f, 1f);
		}

		SetPhaseState (PhaseStates.TurnEnd);
	}
}