﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Data/Work Data")]
public class WorkData : ScriptableObject {
	public string LogPrefix {
		get { return "<b>[" + name + "]: </b>"; }
	}

	public List<WorkProgressData> dataList = new List<WorkProgressData> ();

	public WorkProgressData GetDataFromLevel (int level) {
		int index = level - 1;

		if (index < 0 || index >= dataList.Count) {
			Debug.LogWarning (LogPrefix + "Level is out of array: " + level);
			return null;
		}

		return dataList[index];
	}

	public void UpdatePlayerValue (PlayerData data, int level) {
		var d = GetDataFromLevel (level);

		data.GoldEarned (d.goldChanged);
		data.happiness.current += d.happinessChanged;

		Debug.Log (LogPrefix + " Gold Changed: " + d.goldChanged);
		Debug.Log (LogPrefix + " Happiness Changed: " + d.happinessChanged);
	}
}

[System.Serializable]
public class WorkProgressData {
	public int goldChanged;
	public int happinessChanged;
	public int amountToLvUp;
	public int livingCost;
	[Range (0, 1)]
	public float probablityToGetFans = 0f;

	public bool CheckCondition (int amount) {
		return amount >= amountToLvUp;
	}

	public bool GetFans () {
		// return true;
		return UnityEngine.Random.value <= probablityToGetFans;
	}
}