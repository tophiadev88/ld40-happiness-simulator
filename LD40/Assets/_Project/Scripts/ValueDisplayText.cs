﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

[SelectionBase]
public class ValueDisplayText : MonoBehaviour {
	public Color addColor = Color.yellow;
	public Color subtractColor = Color.red;
	public int numberToDisplay = 0;
	public float moveY = 3f;
	public float duration = 1f;

	private Vector3 _sp;
	private Text text;
	private void Awake () {
		text = GetComponent<Text> ();
		_sp = transform.localPosition;
	}

	[ContextMenu ("Show")]
	public void Show () {
		transform.DOKill ();
		text.DOKill ();

		transform.localPosition = _sp;

		text.color = (numberToDisplay >= 0) ? addColor : subtractColor;
		var prefix = (numberToDisplay >= 0) ? "+" : string.Empty;
		text.text = prefix + numberToDisplay.KiloFormat ();

		transform.DOLocalMoveY (moveY, duration).SetEase (Ease.OutQuad);
		text.DOFade (0f, duration * 0.5f).SetEase (Ease.OutCirc).SetDelay (duration * 0.5f);
	}

	// private void Update() {
	// 	text.color = (numberToDisplay >= 0) ? addColor : subtractColor;
	// 	var prefix = (numberToDisplay >= 0) ? "+" : string.Empty;
	// 	text.text = prefix + numberToDisplay.KiloFormat ();
	// }
}