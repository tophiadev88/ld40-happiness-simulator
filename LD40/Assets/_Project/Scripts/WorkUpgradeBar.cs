﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[SelectionBase]
public class WorkUpgradeBar : MonoBehaviour {
	public float cv;
	public float tv;
	[Range (0f, 1f)]
	public float damp = 0.5f;
	public float maxVal = 1f;
	public float minVal = 0f;

	public Image fillImg;

	private void Awake() {
		fillImg = transform.Find ("Fill").GetComponent<Image> ();
	}

	private void Update() {
		tv = Mathf.Clamp (tv, 0f, maxVal);
		
		cv = Mathf.Lerp (cv, tv, damp);

		if (Mathf.Abs (tv - cv) <= 0.01f) cv = tv;

		fillImg.fillAmount = GetProgress (cv);
	}

	public void SetValue (float v) {
		tv = v;
	}

	public void SetRange (float min, float max) {
		minVal = min;
		maxVal = max;
	}
	
	public float GetProgress (float v) {
		return (v - minVal) / (maxVal - minVal);
	}
}
