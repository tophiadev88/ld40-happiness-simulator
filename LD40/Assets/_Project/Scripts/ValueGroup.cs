﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ValueGroup : MonoBehaviour {
	private Image image;
	private Text amountTxt;

	private void Awake () {
		image = GetComponentInChildren<Image> ();
		amountTxt = GetComponentInChildren<Text> ();
	}
	public ValueGroup Init (ValueTypes types, int amount) {
		image.sprite = GameData.GetSpriteFromType (types);
		var amountStr = amount.KiloFormat ();
		amountTxt.text = (amount > 0) ? "+" + amountStr : amountStr;
		if (amount <= 0 || types == ValueTypes.MaxHappiness) {
			amountTxt.color = Color.red;
		}
		return this;
	}
}