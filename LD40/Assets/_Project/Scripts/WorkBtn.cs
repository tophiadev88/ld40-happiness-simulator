﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WorkBtn : MonoBehaviour {
	public Transform descGroup;
	public Text titleTxt;
	public Text lvlTxt;
	public Action onClick;
	public Action onLvUp;
	public WorkUpgradeBar expBar;

	private void Awake () {
		descGroup = transform.Find ("DescGroup");
		titleTxt = transform.Find ("TitleTxt").GetComponent<Text> ();
		lvlTxt = transform.Find ("UpgradeBar/LvlTxt").GetComponent<Text> ();
		expBar = GetComponentInChildren<WorkUpgradeBar> ();

		descGroup.ClearChild ();
	}

	public void Init (string name) {
		titleTxt.text = name;
	}

	public void UpdateData (WorkProgressData data) {
		descGroup.ClearChild ();

		if (data.goldChanged != 0) {
			GameData.CreateWorkValueGroup (ValueTypes.Gold, data.goldChanged, descGroup);
		}

		if (data.happinessChanged != 0) {
			GameData.CreateWorkValueGroup (ValueTypes.Happiness, data.happinessChanged, descGroup);
		}
	}

	public void SetLv (int lvl) {
		lvlTxt.text = "LV. " + lvl.ToString ();
	}

	public void OnClick () {
		if (onClick != null) onClick ();
	}
}