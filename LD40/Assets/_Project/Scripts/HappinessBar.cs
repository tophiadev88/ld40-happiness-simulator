﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HappinessBar : MonoBehaviour {
	public Slider slider;
	public Text progressTxt;

	private void Awake () {
		slider = GetComponent<Slider> ();
		progressTxt = GetComponentInChildren<Text> ();
	}

	private void Update () {
		progressTxt.text = string.Format ("{0}/{1}", slider.value, slider.maxValue);
	}

	public void SetValue (int h) {
		slider.value = h;
	}

	public void SetMaxValue (int m) {
		slider.maxValue = m;
	}

	public void Set (Happiness happiness) {
		SetMaxValue (happiness.max);
		SetValue (happiness.current);
	}
}