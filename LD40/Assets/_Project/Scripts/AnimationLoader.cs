﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationLoader : MonoBehaviour {
	public Sprite[] sprites;
	public SpriteRenderer[] spriteRenderers;
	[Range (0,1)]
	public float alpha = 1f;

	private void Awake() {
		spriteRenderers = GetComponentsInChildren<SpriteRenderer> ();
	}

	[ContextMenu ("Generate")]
	public void Init (Sprite[] sprites) {
		this.sprites = sprites;
		foreach (var s in sprites) {
			foreach (var r in spriteRenderers) {
				if (r.sprite.name == s.name) {
					r.sprite = s;
					continue;
				}
			}
		}
	}

	private void Update() {
		foreach (var s in spriteRenderers) {
			var c = s.color;
			c.a = alpha;
			s.color = c;
		}

	}
}
