﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameUtility {
	public static void ClearChild (this Transform t) {
		for (int i = 0; i < t.childCount; i++) {
			GameObject.Destroy (t.GetChild (i).gameObject);
		}
	}

	public static string ToListString<T> (this List<T> list) {
		Debug.Assert (list != null && list.Count != 0, "List Error");

		var str = list[0].ToString ();
		for (int i = 1; i < list.Count; i++) {
			str += ", " + list[i].ToString ();
		}

		return str;
	}

	public static string KiloFormat (this int num) {
		var prefix = string.Empty;

		if (num < 0) prefix = "-";

		num = Mathf.Abs (num);

		var retval = num.ToString ("#,0");

		if (num >= 100000000)
			retval = (num / 1000000).ToString ("#,0M");

		else if (num >= 10000000)
			retval = (num / 1000000).ToString ("0.#") + "M";

		else if (num >= 100000)
			retval = (num / 1000).ToString ("#,0K");

		else if (num >= 10000)
			retval = (num / 1000).ToString ("0.#") + "K";

		return prefix + retval;
	}
}