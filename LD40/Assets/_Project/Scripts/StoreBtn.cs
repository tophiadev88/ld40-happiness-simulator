﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class StoreBtn : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler {
	public ItemTypes itemTypes;
	public int id;
	public bool isSelected = false;
	public Text nameTxt;
	public Text goldTxt;
	public Transform hGroup;
	public Action<StoreBtn> onPointerEnter;
	public Action<StoreBtn> onPointerClick;
	public Action<StoreBtn> onPointerExit;
	public Image image;

	private void Awake () {
		nameTxt = transform.Find ("TitleTxt").GetComponent<Text> ();
		hGroup = transform.Find ("HGroup");
		image = GetComponent<Image> ();

		hGroup.ClearChild ();
	}

	public StoreBtn Init (ItemTypes itemTypes, ItemData itemData) {
		this.itemTypes = itemTypes;
		this.id = itemData.id;

		var e = itemData.instantEffect;

		if (itemData.condition.currentGold != 0) {
			GameData.CreateStoreValueGroup (ValueTypes.Gold, e.gold, hGroup);
		}

		if (itemData.instantEffect.happiness != 0) {
			GameData.CreateStoreValueGroup (ValueTypes.Happiness, e.happiness, hGroup);
		}

		nameTxt.text = itemData.itemName;

		return this;
	}

	public void OnPointerEnter (PointerEventData eventData) {
		if (onPointerEnter != null) onPointerEnter (this);
	}

	public void OnPointerExit (PointerEventData eventData) {
		if (onPointerExit != null) onPointerExit (this);
	}

	public void OnPointerClick (PointerEventData eventData) {
		if (onPointerClick != null) onPointerClick (this);
	}
}