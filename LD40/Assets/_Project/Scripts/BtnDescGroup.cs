﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[SelectionBase]
public class BtnDescGroup : MonoBehaviour {
	public Text titleTxt, descTxt;
	public Transform effPerTurnVGroup;
	public Transform instantEffVGroup;

	private void Awake () {
		titleTxt = transform.Find ("TitleTxt").GetComponent<Text> ();
		descTxt = transform.Find ("DescTxt").GetComponent<Text> ();

		effPerTurnVGroup = transform.Find ("EffPerTurnGroup/VGroup");
		instantEffVGroup = transform.Find ("InstantEffGroup/VGroup");

		Clear ();
	}

	public void Display (ItemTypes itemTypes, int id) {
		Clear ();

		var data = GameData.GetItemData (itemTypes, id);
		titleTxt.text = data.itemName;
		descTxt.text = data.itemDesc;

		SetupEffPerTurn (data.effectPerTurn);
		SetupInstantEff (data.instantEffect);
	}

	private void Clear () {
		effPerTurnVGroup.ClearChild ();
		instantEffVGroup.ClearChild ();
	}

	private void SetupInstantEff (InstantEffect instantEffect) {
		if (instantEffect.happiness != 0) {
			GameData.CreateDescValueGroup (ValueTypes.Happiness, instantEffect.happiness, instantEffVGroup);
		}
		if (instantEffect.maxHappiness != 0) {
			GameData.CreateDescValueGroup (ValueTypes.MaxHappiness, instantEffect.maxHappiness, instantEffVGroup);
		}
	}

	private void SetupEffPerTurn (EffectPerTurn effectPerTurn) {
		if (effectPerTurn.gold != 0) {
			GameData.CreateDescValueGroup (ValueTypes.Gold, effectPerTurn.gold, effPerTurnVGroup);
		}

		if (effectPerTurn.happiness != 0) {
			GameData.CreateDescValueGroup (ValueTypes.Happiness, effectPerTurn.happiness, effPerTurnVGroup);
		}
	}
}