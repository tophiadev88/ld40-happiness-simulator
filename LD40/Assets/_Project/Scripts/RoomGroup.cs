﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomGroup : MonoBehaviour {
	public float spacingZ;
	public float spacingX;
	public float width;
	public int amountPerLine = 7;
	public LayerMask layer;

	private void Update () {
		for (int i = 0; i < transform.childCount; i++) {
			var t = transform.GetChild (i);
			int z = i / (amountPerLine - 1);
			int x = i % (amountPerLine - 1);

			var origin = transform.position;
			origin.x -= (spacingX * (amountPerLine - 1)) * 0.5f;

			t.localPosition = origin + new Vector3 (x * spacingX, 0, z * spacingZ);
		}
	}
}