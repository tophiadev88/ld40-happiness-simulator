﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameData {
	private static GameData _Instance;
	public WorkData moneyData;
	public WorkData dreamData;

	public ItemData[] girlDatas;
	public ItemData[] carDatas;
	public ItemData[] houseDatas;
	public ItemData[] fanDatas;

	public static void Init () {
		if (_Instance != null) return;

		_Instance = new GameData ();
		_Instance.InitData ();
	}

	private void InitData () {
		_Instance.moneyData = Resources.Load<WorkData> ("Data/ForTheMoney");
		_Instance.dreamData = Resources.Load<WorkData> ("Data/ForTheDream");

		girlDatas = Resources.LoadAll<ItemData> ("Data/Items/Girls");
		carDatas = Resources.LoadAll<ItemData> ("Data/Items/Cars");
		houseDatas = Resources.LoadAll<ItemData> ("Data/Items/Houses");
		fanDatas = Resources.LoadAll<ItemData> ("Data/Items/Fans");
	}

	public static Sprite GetSpriteFromType (ValueTypes types) {
		if (types == ValueTypes.Gold)
			return Resources.Load<Sprite> ("ui-gold");

		if (types == ValueTypes.MaxHappiness)
			return Resources.Load<Sprite> ("ui-happiness-max");
			
		return Resources.Load<Sprite> ("ui-happiness");
	}

	public static ValueGroup CreateDescValueGroup (ValueTypes types, int amount, Transform parent) {
		return CreateValueGroup ("DescValueGroup", types, amount, parent);
	}

	public static ValueGroup CreateStageValueGroup (ValueTypes types, int amount, Transform parent) {
		return CreateValueGroup ("StageValueGroup", types, amount, parent);
	}

	public static ValueGroup CreateWorkValueGroup (ValueTypes types, int amount, Transform parent) {
		return CreateValueGroup ("WorkValueGroup", types, amount, parent);
	}

	public static ValueGroup CreateStoreValueGroup (ValueTypes types, int amount, Transform parent) {
		return CreateValueGroup ("StoreDisplayValueGroup", types, amount, parent);
	}

	public static ValueGroup CreateValueGroup (string path, ValueTypes types, int amount, Transform parent) {
		var p = CreatePrefab<ValueGroup> (path).Init (types, amount);
		p.transform.SetParent (parent, false);
		return p;
	}

	public static GameObject CreateStoreBanner (string title) {
		var obj = CreatePrefab<GameObject> ("StoreBanner");
		obj.GetComponentInChildren<Text> ().text = title;
		return obj;
	}

	public static ItemData GetItemData (ItemTypes itemTypes, int id) {
		if (itemTypes == ItemTypes.Girl) return _Instance.girlDatas[id];
		if (itemTypes == ItemTypes.Car) return _Instance.carDatas[id];
		if (itemTypes == ItemTypes.House) return _Instance.houseDatas[id];
		if (itemTypes == ItemTypes.Fan) return _Instance.fanDatas[id];
		return null;
	}

	public static StoreBtn CreateStoreBtn (ItemTypes itemTypes, ItemData itemData) {
		return CreatePrefab<StoreBtn> ("StoreBtn").Init (itemTypes, itemData);
	}

	public static T CreatePrefab<T> (string path) where T : Object {
		T rsc = Resources.Load<T> (path);
		Debug.Assert (rsc != null, "MISSING PREFAB: " + path);
		return GameObject.Instantiate (rsc) as T;
	}

	public static Item CreateItem (ItemTypes itemTypes, int id, Transform parent) {
		var d = GetItemData (itemTypes, id);
		var prefab = CreatePrefab<Item> (itemTypes.ToString ()).Init (itemTypes, d);
		prefab.transform.SetParent (parent);
		return prefab;
	}

	public static WorkData GetMoneyWork () {
		return _Instance.moneyData;
	}

	public static WorkData GetDreamWork () {
		return _Instance.dreamData;
	}

	public static ItemData[] GetGirlDatas () {
		return _Instance.girlDatas;
	}

	public static ItemData[] GetCarDatas () {
		return _Instance.carDatas;
	}

	public static ItemData[] GetHouseDatas () {
		return _Instance.houseDatas;
	}
}