﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class PopupGroup : MonoBehaviour {
	public Text text;
	public CanvasGroup canvasGroup;

	private void Awake () {
		text = GetComponentInChildren<Text> ();
		canvasGroup = GetComponent<CanvasGroup> ();
		canvasGroup.alpha = 0f;
	}

	public void ShowText (string txt) {
		text.text = txt;
		Animate ();
	}

	[ContextMenu ("Animate")]
	public void Animate () {
		canvasGroup.DOKill ();
		canvasGroup.DOFade (1f, 1f);
		canvasGroup.DOFade (0f, 2f).SetDelay (3f);
	}
}