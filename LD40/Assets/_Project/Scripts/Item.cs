﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Item : MonoBehaviour {
	public int id;
	public ItemTypes itemTypes;
	public Sprite[] sprites;
	public SpriteRenderer[] spriteRenderers;
	public string path;
	public Transform vGroup;
	public Transform gui;
	public CanvasGroup canvasGroup;
	private Animator _animator;
	private Vector3 _uiPos;

	private void Awake () {
		spriteRenderers = GetComponentsInChildren<SpriteRenderer> ();
		_animator = GetComponent<Animator> ();
		
		gui = transform.Find ("GUI");
		canvasGroup = gui.GetComponent<CanvasGroup> ();
		vGroup = transform.Find ("GUI/VGroup");

		_uiPos = gui.transform.localPosition;
		canvasGroup.alpha = 0f;		
		vGroup.ClearChild ();
	}

	[ContextMenu ("Load Path")]
	public void LoadPath () {
		sprites = Resources.LoadAll<Sprite> (path);

		foreach (var s in sprites) {
			foreach (var r in spriteRenderers) {
				if (r.sprite.name == s.name) {
					r.sprite = s;
					continue;
				}
			}
		}
	}

	public Item Init (ItemTypes itemTypes, ItemData itemData) {
		this.itemTypes = itemTypes;
		this.id = itemData.id;

		sprites = Resources.LoadAll<Sprite> (itemTypes + "/" + itemData.itemName);

		foreach (var s in sprites) {
			foreach (var r in spriteRenderers) {
				if (r.sprite.name == s.name) {
					r.sprite = s;
					continue;
				}
			}
		}

		return this;
	}

	public void Triggered (EffectPerTurn effectPerTurn) {
		_animator.SetTrigger ("EffPerTurn");
		PopText (effectPerTurn);
	}

	private void PopText (EffectPerTurn effectPerTurn) {
		vGroup.ClearChild ();
		
		if (effectPerTurn.happiness != 0) {
			GameData.CreateStageValueGroup (ValueTypes.Happiness, effectPerTurn.happiness, vGroup);
		}

		if (effectPerTurn.gold != 0) {
			GameData.CreateStageValueGroup (ValueTypes.Gold, effectPerTurn.gold, vGroup);
		}

		UpdatePos ();
	}

	public void UpdatePos () {
		DOTween.Kill (gui);
		DOTween.Kill (canvasGroup);

		gui.transform.localPosition = _uiPos;
		canvasGroup.alpha = 1f;

		gui.transform.DOLocalMoveY (5.84f, 1f) .SetEase (Ease.OutQuad);
		canvasGroup.DOFade (0f, 1f).SetDelay (0.5f);
	}

	public void Leave () {
		_animator.SetTrigger ("Leave");
		Destroy (gameObject, 10f);
	}

	public void Fade (float alpha) {
		foreach (var s in spriteRenderers) {
			s.DOKill ();
			s.DOFade (alpha, 1f);
		}
	}
}