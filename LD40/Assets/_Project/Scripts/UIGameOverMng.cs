﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class UIGameOverMng : MonoBehaviour {
	public const string LogPrefix = "<b>[UIGameOverMng]:</b> ";
	public Camera uiCamera;
	public CanvasGroup canvasGroup;
	public GameObject depressionGroup;
	public GameObject bankruptGroup;
	public Button restartBtn;
	public Action onClickRestartBtn;

	private void Awake () {
		canvasGroup = GetComponent<CanvasGroup> ();
		uiCamera = GetComponent<Canvas> ().worldCamera;

		depressionGroup = transform.Find ("DepressionGroup").gameObject;
		bankruptGroup = transform.Find ("BankruptGroup").gameObject;

		restartBtn = transform.Find ("RestartBtn").GetComponent<Button> ();

		canvasGroup.alpha = 0f;
		canvasGroup.interactable = false;
		canvasGroup.blocksRaycasts = false;

		uiCamera.gameObject.SetActive (false);

		depressionGroup.SetActive (false);
		bankruptGroup.SetActive (false);
	}

	public void SetGameOver (GameStates gameStates) {
		Debug.Log (LogPrefix + "Game Over: " + gameStates);

		canvasGroup.interactable = true;
		canvasGroup.blocksRaycasts = true;

		uiCamera.gameObject.SetActive (true);

		switch (gameStates) {
			case GameStates.GameEndedWithZeroHappiness:
				depressionGroup.SetActive (true);
				break;

			case GameStates.GameEndedWithZeroGold:
				bankruptGroup.SetActive (true);
				break;
		}

		canvasGroup.DOFade (1f, 1f);
	}
}