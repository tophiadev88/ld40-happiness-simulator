﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.PostProcessing;

public class CameraFollow : MonoBehaviour {

	public Transform target;
	public Vector3 targetOffset;
	[HideInInspector]
	public Vector3 defaultPos;
	public float smoothTimer = 1f;
	private Vector3 _velocity;

	public PostProcessingProfile defaultProfile;
	public PostProcessingProfile dangerProfile;

	private void Awake () {
		defaultPos = transform.position;
	}

	public void SetTarget (Transform target) {
		this.target = target;

		var items = FindObjectsOfType<Item> ();

		if (target != null) {
			foreach (var i in items) {
				if (i.transform == target) continue;
				i.Fade (0f);
			}
		}
		else {
			foreach (var i in items) {
				i.Fade (1f);
			}
		}
	}

	public void SetDanger (bool enabled) {
		if (enabled) {
			GetComponent<PostProcessingBehaviour> ().profile = dangerProfile;
		} else {
			GetComponent<PostProcessingBehaviour> ().profile = defaultProfile;
		}
	}

	private void Update () {
		var pos = defaultPos;

		if (target != null) {
			pos = target.position + targetOffset;
		}

		transform.position = Vector3.SmoothDamp (transform.position, pos, ref _velocity, smoothTimer);
	}
}