﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerData {
	public const string LogPrefix = "<b>[Player]:</b> ";
	public int levelForMoney = 1;
	public int levelForDream = 1;
	public int workForDreamAmount = 0;
	public int workForMoneyAmount = 0;
	public int currentGold = 300;
	public int spentGold = 0;
	public int earnedGold = 0;
	public bool carBought = false;
	public bool girlfriend = false;
	public LivingCost livingCost = new LivingCost (-100, 0);
	public Happiness happiness = new Happiness (50, 100);
	public List<Item> ownedGirls = new List<Item> ();
	public List<Item> ownedHouses = new List<Item> ();
	public List<Item> ownedCars = new List<Item> ();
	public List<Item> ownedItems = new List<Item> ();
	public List<Item> ownedFans = new List<Item> ();

	public void UpdatePerTurn (GameMng gameMng) {
		GoldSpend (livingCost.gold);
		happiness.current += livingCost.happiness;
	}

	public void GoldSpend (int gold) {
		currentGold += gold;
		if (gold < 0) {
			spentGold -= gold;
		}
	}

	public void GoldEarned (int gold) {
		currentGold += gold;
		earnedGold += gold;
	}

	public void AddItems (Item item) {
		ownedItems.Add (item);
		GetListFromItemTypes (item.itemTypes).Add (item);
	}

	public void RemoveItems (Item item) {
		ownedItems.Remove (item);
		GetListFromItemTypes (item.itemTypes).Remove (item);
		var d = GameData.GetItemData (item.itemTypes, item.id);
		livingCost.gold -= d.effectPerTurn.gold;
		livingCost.happiness -= d.effectPerTurn.happiness;
		Penalty (d.penalty);
	}

	private void Penalty (PenaltyEffect effect) {
		happiness.current += effect.happiness;
		happiness.max += effect.maxHappiness;
	}

	public bool CheckOwnedBadThing () {
		return ownedCars.Count > 0 || ownedGirls.Count > 0;
	}

	public List<Item> GetListFromItemTypes (ItemTypes itemTypes) {
		if (itemTypes == ItemTypes.Girl) return ownedGirls;
		if (itemTypes == ItemTypes.Car) return ownedCars;
		if (itemTypes == ItemTypes.House) return ownedHouses;
		if (itemTypes == ItemTypes.Fan) return ownedFans;
		return null;
	}

	public void ApplyEffectPerTurn (EffectPerTurn effPerTurn) {
		livingCost.gold += effPerTurn.gold;
		livingCost.happiness += effPerTurn.happiness;
	}

	public void ApplyInstantEffect (InstantEffect instantEffect) {
		happiness.current += instantEffect.happiness;
		happiness.max += instantEffect.maxHappiness;
		GoldSpend (instantEffect.gold);

		Debug.Log (LogPrefix + "Effect Applied!");
	}
}

[System.Serializable]
public class LivingCost {
	public int gold;
	public int happiness;

	public LivingCost (int gold, int happiness) {
		this.gold = gold;
		this.happiness = happiness;
	}
}

[System.Serializable]
public class Happiness {
	public int max;
	public int current;

	public Happiness (int current, int max) {
		this.current = current;
		this.max = max;
	}
}