﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Data/Item Data")]
public class ItemData : ScriptableObject {
	public int id;
	public string itemName;
	[TextArea (3, 10)]
	public string itemDesc;
	public InstantEffect instantEffect;
	public PenaltyEffect penalty;
	public EffectPerTurn effectPerTurn;
	public SellingInfo sellable;
	public ItemCondition condition;

	public List<string> CheckCondition (PlayerData playerData) {
		var retval = new List<string> ();

		if (playerData.currentGold < condition.currentGold)
			retval.Add ("You do not have enough gold!");

		if (playerData.levelForMoney < condition.moneyLv)
			retval.Add ("Your income is too low!");

		if (playerData.ownedHouses.Count < condition.amountOfHouses)
			retval.Add ("Don't talk to me, you don't even have a house!");

		if (condition.amountOfGirlfriends >= 0 &&
		    playerData.ownedGirls.Count > condition.amountOfGirlfriends)
			retval.Add ("Leave me alone, Playboy!");

		if (playerData.ownedCars.Count < condition.amountOfCars)
			retval.Add ("You have no even a car, how dare you talk to me?");

		return retval;
	}
}

[System.Serializable]
public struct SellingInfo {
	// -1 is not sellable, 0 is able to sell but no gold gained
	public int dayAbleToSell;
	public int valueChangedPerTurn;
}

[System.Serializable]
public struct InstantEffect {
	public int gold;
	public int happiness;
	public int maxHappiness;
}

[System.Serializable]
public struct EffectPerTurn {
	public int gold;
	public int happiness;
}

[System.Serializable]
public struct PenaltyEffect {
	public int happiness;
	public int maxHappiness;
}

[System.Serializable]
public struct ItemCondition {
	public int currentGold;
	public int moneyLv;
	public int amountOfHouses;
	public int amountOfGirlfriends;
	public int amountOfCars;
}