﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class UIMng : MonoBehaviour {
	public WorkBtn mBtn, dBtn;
	public Text cgTxt;
	public Text turnTxt;
	public Text lvcGTxt, lvcHTxt;
	public HappinessBar happinessBar;
	public Transform storeVGroup;
	public BtnDescGroup descGroup;
	public StoreBtn selectedStoreBtn;
	public ValueDisplayText goldCostDisplay;
	public ValueDisplayText happinessCostDisplay;
	public GameObject gameFinishGroup;
	public PopupGroup popupGroup;
	public Image gAlertImg, hAlertImg;
	public Image slashA;
	public Image slashB;
	public Image circleImg;
	public Image questionImg;
	// public Button mLvUpBtn, dLvUpBtn;

	public List<StoreBtn> storeBtns = new List<StoreBtn> ();

	public Action<ItemTypes, int> onBuyItem;

	private Rigidbody2D rg;

	private void Awake () {
		// Init Buttons
		mBtn = transform.Find ("BtnGroup/FTMBtn").GetComponent<WorkBtn> ();
		dBtn = transform.Find ("BtnGroup/FTDBtn").GetComponent<WorkBtn> ();

		descGroup = GetComponentInChildren<BtnDescGroup> ();

		// Init Texts
		cgTxt = transform.Find ("TopBanner/CurrentGoldGroup/AmountTxt").GetComponent<Text> ();

		turnTxt = transform.Find ("TopBanner/TurnGroup/AmountTxt").GetComponent<Text> ();

		lvcGTxt = transform.Find ("LivingCostGroup/GoldCostGroup/ValueTxt").GetComponent<Text> ();
		lvcHTxt = transform.Find ("LivingCostGroup/HappinessCostGroup/ValueTxt").GetComponent<Text> ();

		storeVGroup = transform.Find ("StoreMenu/Viewport/Content");
		storeVGroup.ClearChild ();

		goldCostDisplay = transform.Find ("TopBanner/CurrentGoldGroup").GetComponentInChildren<ValueDisplayText> ();
		happinessCostDisplay = transform.Find ("TopBanner/HappinessGroup").GetComponentInChildren<ValueDisplayText> ();
		gAlertImg = transform.Find ("TopBanner/CurrentGoldGroup/AlertImg").GetComponent<Image> ();
		hAlertImg = transform.Find ("TopBanner/HappinessGroup/AlertImg").GetComponent<Image> ();
		
		gameFinishGroup = transform.Find ("GameFinishGroup").gameObject;

		gAlertImg.DOFade (0f, 0f);
		hAlertImg.DOFade (0f, 0f);

		popupGroup = GetComponentInChildren<PopupGroup> ();

		happinessBar = GetComponentInChildren<HappinessBar> ();

		slashA.DOFade (0f, 0f);
		slashB.DOFade (0f, 0f);

		circleImg.DOFade (0f, 0f);
		questionImg.DOFade (0f, 0f);

		descGroup.gameObject.SetActive (false);
		gameFinishGroup.SetActive (false);

		rg = GetComponentInChildren<Rigidbody2D> ();
	}

	public void SetTurnText (int turn) {
		turnTxt.text = turn.ToString ();
	}

	public void SetCurrentGold (int gold) {
		cgTxt.text = gold.ToString ();
	}

	public void AddItems (ItemTypes itemTypes, ItemData[] itemDatas) {
		var banner = GameData.CreateStoreBanner (itemTypes.ToString ());
		banner.transform.SetParent (storeVGroup, false);

		foreach (var d in itemDatas) {
			var btn = GameData.CreateStoreBtn (itemTypes, d);
			btn.transform.SetParent (storeVGroup, false);

			// Setup Callback
			btn.onPointerEnter = OnEnterStoreBtn;
			btn.onPointerExit = OnExitStoreBtn;
			btn.onPointerClick = OnClickStoreBtn;

			storeBtns.Add (btn);
		}
	}

	private void OnClickStoreBtn (StoreBtn btn) {
		if (onBuyItem != null) onBuyItem (btn.itemTypes, btn.id);
	}

	private void OnExitStoreBtn (StoreBtn btn) {
		if (selectedStoreBtn == btn)
			SetSelectedStoreBtn (null);
	}

	private void OnEnterStoreBtn (StoreBtn btn) {
		SetSelectedStoreBtn (btn);
	}

	private void SetSelectedStoreBtn (StoreBtn btn) {
		if (selectedStoreBtn != null) {
			selectedStoreBtn.isSelected = false;
		}

		selectedStoreBtn = btn;

		descGroup.gameObject.SetActive (selectedStoreBtn != null);

		if (selectedStoreBtn != null) {
			descGroup.Display (selectedStoreBtn.itemTypes, selectedStoreBtn.id);
			selectedStoreBtn.isSelected = true;
		}
	}

	public void GAlert (bool enabled) {
		gAlertImg.DOKill ();
		if (enabled) {
			gAlertImg.DOFade (1f, 1f).SetLoops (-1, LoopType.Yoyo);
		} else {
			gAlertImg.DOFade (0f, 1f);
		}
	}

	public void HAlert (bool enabled) {
		hAlertImg.DOKill ();
		if (enabled) {
			hAlertImg.DOFade (1f, 1f).SetLoops (-1, LoopType.Yoyo);
		} else {
			hAlertImg.DOFade (0f, 1f);
		}
	}

	public void SetLivingCost (LivingCost livingCost) {
		lvcGTxt.text = livingCost.gold.KiloFormat ();
		lvcHTxt.text = livingCost.happiness.ToString ();
	}

	public void SetInteractable (bool enabled) {
		GetComponent<CanvasGroup> ().interactable = enabled;
		if (!enabled)
			SetSelectedStoreBtn (null);
	}

	public void DropFaskToDoList () {
		StartCoroutine (DropFakeCoroutine ());
	}

	public void GameFinish () {
		gameFinishGroup.SetActive (true);
	}

	IEnumerator DropFakeCoroutine() {
		rg.bodyType = RigidbodyType2D.Dynamic;
		yield return new WaitForSeconds (0.5f);
		circleImg.DOFade (1f, 1f);
	}
}